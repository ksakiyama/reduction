__kernel
void reduce(
	__global uint *dst, // 1 element
	__global uint const *src,
	__local  uint *local_mem,
	const uint size)
{
	const uint gid = get_global_id(0);
	const uint blockSize = get_global_size(0);

	local_mem[gid] = src[gid];

	for (uint stride = gid + blockSize; stride < size; stride += blockSize) {
		local_mem[gid] += src[stride];
	}
	
	barrier(CLK_LOCAL_MEM_FENCE);
	
	for (uint i = blockSize >> 1; i > 0; i >>= 1) {
		if (gid < i) {
			local_mem[gid] += local_mem[gid + i];
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	
	if (gid == 0) {
		dst[0] = local_mem[0];
	}
}
