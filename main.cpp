#define __CL_ENABLE_EXCEPTIONS

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <CL/cl.hpp>

const cl_uint size = 1000 * 1000;
cl_uint platform_index = 0;
cl_uint device_index = 0;

unsigned int xors_random() {
	static unsigned int xors_x = 123456789;
	static unsigned int xors_y = 362436069;
	static unsigned int xors_z = 521288629;
	static unsigned int xors_w = 88675123;
	unsigned int t;
	t = (xors_x ^ (xors_x << 11));
	xors_x = xors_y; xors_y = xors_z; xors_z = xors_w; 
	xors_w = (xors_w ^ (xors_w >> 19)) ^ (t ^ (t >> 8));
	return xors_w;
}

double getMiriSec(const cl::Event &ev) {
	cl_ulong start, end;
	ev.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	ev.getProfilingInfo(CL_PROFILING_COMMAND_END  , &end  );
	return (end - start)/1000000.0;
}

int main(int argc, char* argv[]) {
	std::vector<cl_uint> src(size, 0);
	cl_uint dst;

	for (cl_uint i = 0; i < size; i++) {
		src[i] = xors_random() % 10000;
	}

	try {
		/* Platform */
		std::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);
		if (platforms.empty()) {
			std::cerr << "No platform!" << std::endl;
			return EXIT_FAILURE;
		}

		if (platform_index >= platforms.size()) {
			std::cerr << "Ivalid platform index. Platform[0] automatically selected\n";
			platform_index = 0;
		}

		/* Device */
		std::vector<cl::Device> devices;
		platforms[platform_index].getDevices(CL_DEVICE_TYPE_ALL, &devices);
		if (devices.empty()) {
			std::cerr << "No device!" << std::endl;
			return EXIT_FAILURE;
		}

		if (device_index >= devices.size()) {
			std::cerr << "Ivalid device index. Device[0] automatically selected\n";
			platform_index = 0;
		}

		std::string pStr, dStr;
		platforms[platform_index].getInfo(CL_PLATFORM_NAME, &pStr);
		devices[device_index].getInfo<std::string>(CL_DEVICE_NAME, &dStr);
		std::cout << "Platform :" << pStr << std::endl;
		std::cout << "Device   :" << dStr << std::endl;

		/* Context Command-queue */
		cl::Context context(devices);
		cl::CommandQueue queue(context, devices[device_index], CL_QUEUE_PROFILING_ENABLE);
		
		/* Source code */
		std::string filename = "reduce.cl";
		std::ifstream kernelFile(filename.c_str(), std::ios::in);
		if (!kernelFile.is_open()) {
			std::cerr << "Failed to open file for reading: " << filename << std::endl;
			return EXIT_FAILURE;
		}
		std::ostringstream oss;
		oss << kernelFile.rdbuf();
		std::string srcStdStr = oss.str();
		const char *kernelSource = srcStdStr.c_str();

		/* Program */
		cl::Program::Sources source(1, std::make_pair(kernelSource, strlen(kernelSource)));
		cl::Program program(context, source);
		program.build(devices);

		/* Kernel */
		cl::Kernel kernel(program, "reduce");

		size_t blockSize;
		devices[device_index].getInfo(CL_DEVICE_MAX_WORK_GROUP_SIZE, &blockSize);
		std::cout << "CL_DEVICE_MAX_WORK_GROUP_SIZE: " << blockSize << std::endl;

		cl::Buffer out(context, CL_MEM_READ_WRITE, sizeof(cl_uint));
		cl::Buffer in(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint) * size, &src[0]);
		
		kernel.setArg(0, out);
		kernel.setArg(1, in);
		kernel.setArg(2, sizeof(cl_uint) * blockSize, NULL);	//	local memory size
		kernel.setArg(3, size);

		cl::Event event_;

		queue.enqueueNDRangeKernel(kernel,
			cl::NullRange,
			cl::NDRange(blockSize),
			cl::NDRange(blockSize),
			NULL,
			&event_);

		event_.wait();
		
		std::cout << "Kernel Time: " << getMiriSec(event_) << "[ms]" << std::endl;

		queue.enqueueReadBuffer(out, CL_TRUE, 0, sizeof(cl_uint), &dst);

		std::cout << "Result(OpenCL): " << dst << std::endl;

	} catch (const cl::Error err) {
		std::cerr << "Error: " << err.what() << "(" << err.err() << ")" << std::endl;
	}

	cl_uint sum = 0;
	for (cl_uint i = 0; i < size; i++) {
		sum += src[i];
	}
	std::cout << "Result(HOST):   " << sum << std::endl;

	return 0;
}
